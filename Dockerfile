FROM registry.gitlab.com/cognovis-5/erp4projects

WORKDIR /var/www/openacs/packages

# Packages to overwrite
ENV PKGS_LIST "webix-portal sencha-portal sencha-assignment sencha-freelance-translation intranet-sencha-tables intranet-trans-invoices intranet-translation intranet-trans-trados intranet-trans-memoq"
ENV PKGS_OLD_LIST "intranet-trans-project-wizard intranet-trans-termbase intranet-freelance intranet-freelance-translation intranet-freelance-invoices"

RUN for pkg in ${PKGS_LIST} ; do echo $pkg \
    && rm -rf $pkg && wget -q https://gitlab.com/cognovis-5/$pkg/-/archive/master/$pkg.tar.gz \
    && tar xfz $pkg.tar.gz && mv ${pkg}-master-* $pkg && rm $pkg.tar.gz ; done && \
    for pkg in ${PKGS_OLD_LIST} ; do echo $pkg \
    && rm -rf $pkg && wget -q https://gitlab.com/cognovis/$pkg/-/archive/master/$pkg.tar.gz \
    && tar xfz $pkg.tar.gz && mv ${pkg}-master-* $pkg && rm $pkg.tar.gz ; done